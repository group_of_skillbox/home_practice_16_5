#include<iostream>
#include<time.h>

using namespace std;

int main()
{
	setlocale(LC_ALL, "ru"); //��� ������ �������� �����
	char NumberOfDay{}; // ��������� ���������� ��� �������� ����� ������

	//////////��� ������� ���� � �������//////////
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	NumberOfDay = buf.tm_mday;

	//////////�������� ���������� ������������� �������//////////
	int ROW{}; // ���������� �����
	cout << "������� ����������� ������������ ������� (N*N), ������� ����� �����: ";
	cin >> ROW;
	int COLUMN = ROW; // ���������� �������� ����� ���������� �����

	int** array = new int* [ROW]; // ������� ���������� ������ ���������� � ����������� ��������� ������ ROW

	for (int i = 0; i < ROW; i++)
	{
		array[i] = new int[COLUMN]; // ������� ���������� ������� � ����������� ��������� ������� COLUMN
	}

	//////////////////////���������� ���������� ������������� �������////////////////////////

	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COLUMN; j++)
		{
			array[i][j] = (i + j);
			cout << array[i][j] << "\t";
		}
		cout <<"\n\n";

		if (ROW == i+1)
		{
			ROW = NumberOfDay % ROW;
			int Sum{};
			cout << "��������� ���:" << endl;
			for (int j = 0; j < COLUMN; j++)
			{
				cout << array[ROW][j] << "\t";
				Sum += array[ROW][j];
			}
			cout << endl;
			cout << "The Sum of elements for the " << ROW << " row is: " << Sum << endl;
		}
	}

	//////////////////////////������� ������������ ������//////////////////////////
	for (int i = 0; i < ROW; i++)
	{
		delete[] array[i];
	}
	delete[] array;
}